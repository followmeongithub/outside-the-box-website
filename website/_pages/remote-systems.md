---
layout: single
title: " "
header:
  image: /assets/images/alt-energy_banner2.png
permalink: /remote-systems/
---

Whether remote means off-grid or just non-local, we have the expertise 
to power, secure, and connect your next project. [Reach out](/contact/) and tell us 
about your project!


<h1>Secure Connectivity</h1>

Remote management and communication is important for many remote systems.
Cellular, long-range wireless, and satellite are all potential options for 
connectivity. VPN or SSH are two common tools we can use to encrypt communications.
Additional security measures will depend on your particular application.

<h1>Remote Power</h1>

Remote systems require remote power solutions. Since most remote energy
generators don't produce constantly (e.g. solar), charging and storage 
solutions must be considered.

![Solar Power](/assets/images/caspar-rae-b6vAiN3wYNw-unsplash_600x371.png)

<br>

