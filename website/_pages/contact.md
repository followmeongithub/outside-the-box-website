---
layout: single
author_profile: true
header:
  overlay_image: /assets/images/alex-bierwagen-Uuz7yti7SQA-unsplash.jpg
permalink: /contact/
---




<form
  action="https://formspree.io/f/mpzvnykv"
  method="POST"
>
  <label for="email" class="required">
    Your email:
  </label>
  <input type="email" name="email" required>
  <label style="font-weight: bold;">
    Your Phone Number:
  </label>
  <input type="tel" id="phone" name="phone" pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}">
  <label style="font-weight: bold;">How can we help?</label>
  <label>Custom Development</label>
  <input type="checkbox" name="custom-development">
  <label>Service-Free</label>
  <input type="checkbox" name="service-free">
  <label>Remote Systems</label>
  <input type="checkbox" name="remote-systems">
  <label>Reverse Engineering</label>
  <input type="checkbox" name="reverse-engineering">
  <label>Other</label>
  <input type="checkbox" name="other">

  <label style="font-weight: bold;">
    Your message:
    <textarea rows=10 name="message"></textarea>
  </label>
  <!-- your other form fields go here -->
  <button type="submit">Send</button>
</form>

<style>
input[type="checkbox"] {
  position: relative;
  float: inline-end;
  margin-top: -1.5em;
}

</style>
