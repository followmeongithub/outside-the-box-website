---
layout: single
author_profile: true
title: "BB Biz LLC"
header:
  overlay_image: /assets/images/off-grid_banner.png
permalink: /about/
---

BB Biz LLC operates <b>Outside the Box</b>, approaching problems a little bit different.
We like to keep solutions simple, knowledge open, and "doing business" affordable.

We use and promote open source technologies which are community driven 
and free to use and modify. A large portion of the internet is
running on open source software, but licensing and service fees have become
standard business.

We aim to enable organizations to benefit more directly from community driven
efforts and cut down on service-fee fatigue. As such, the solutions we provide are
royalty-free and yours to keep or share. 

We offer support as-needed on an hourly or retainer basis to ensure you’re not left 
in the dark, but there is no builtin monthly service-fee. Furthermore, we
strive to provide best-in-class documentation so you never need to fear vendor lock-in.

[Contact us](/contact/) to learn more and share how we can help.


