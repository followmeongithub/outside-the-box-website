---
layout: single
title: "Service-Free"
header:
  overlay_image: /assets/images/apps_banner.png
  overlay_filter: 0.7
  image_description: "Services without the fees"
classes: wide
permalink: /service-free/
---

# Software you need - <em>no</em> monthly fees.

![Productivity Software](/assets/images/andreas-klassen-gZB-i-dA6ns-unsplash_600x400.png){: .align-right}

<em>Business productivity services without the monthly or annual fees </em>
means your core business functions don't need to depend on recurring revenue.
Keeping recurring expenses low has become increasingly difficult as the 
market has shifted toward service-based <em>everything</em>. 

We're nudging the needle in the other direction. We want you to own the
systems you depend on so your business can be fiscally sustainable and
autonomous.

Think of it like running a "cloud" on your local office or home
network. The difference is you own your data and your software - no 
more renting access.

<h1>Business Productivity</h1>
Communication and information platforms enable modern businesses 
to move faster with better coordination. In order to keep up with the pace
of business, it's almost essential that you adopt various tools to help
get the job done. Open source tools running on-site allow you to control 
your business data and software budget.


<h1>Modular</h1>

With a large selection of open source software out there, the possibilities 
are almost endless! You can get an idea of some basic types of tools in the list below,
or you can let us know the business challenges you're facing and our team
can help identify open source tools that can address your individual needs.

- [Cloud Platform](https://docs.nextcloud.com/) for document sharing, web portals, and more.
- [Enterprise Password Management](https://github.com/passbolt) for managing and securely 
sharing digital credentials.
- [Customer Relationship Manager(CRM)](https://github.com/odoo/odoo) for tracking and managing customer relationships.
- [Project Management](https://www.openproject.org/community-edition/) for tracking and coordinating projects.
- [Internal Documentation](https://just-the-docs.github.io/just-the-docs/) - a central location to share internal knowledge and workflows.
- [What tools would you like to see?](/contact/)


<h1>The Hardware</h1>

![The Hardware](/assets/images/jainath-ponnala-9wWX_jwDHeM-unsplash_600x400.png){: .align-right}

"Service-Free" is simply a tiny computer installed and configured with 
a collection of open source software selected to enable your business.

These units are compact 6" cubes (or less) and use as low as 5 Watts! 
That means they are ideal for remote applications and the energy conscious.

By simply plugging your device into power and your local network you can
begin using your new suite of software. We can provide guidance and support 
to make it securely accessible across the internet.


<h1>No Vendor Lock-in</h1>

![Horses Roaming Free](/assets/images/louise-pilgaard-l263JvA7q2E-unsplash_600x400.png){: .align-right}

<em>You'll find no gates or fences here; you're free to roam.</em>

We are happy to provide ongoing support services for your new device, but
we provide documentation so you have options. Whether your IT team 
will take the reins or you want to run wild with it yourself, our documentation 
will enable you to use the platform for years to come. 

[Reach out](/contact/) if you'd like to learn more about the benefits of 
being Service-Free.

