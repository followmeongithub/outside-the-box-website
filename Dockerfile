FROM ubuntu:22.04

ENV BUNDLE_PATH=/gembox

RUN apt update && \
    apt install -y \
            build-essential \
            gosu \
            ruby-full \
            webp \
            zlib1g-dev && \
    useradd --create-home \
            -d /home/ruby-user \
            --uid 1000 \
            --shell /bin/bash \
            ruby-user && \
    chown -R ruby-user:1000 /home/ruby-user && \
    chmod -R 755 /home/ruby-user &&\
    mkdir -p $BUNDLE_PATH && \
    chown -R ruby-user:1000 $BUNDLE_PATH


RUN gem install bundler \
                jekyll \
                jekyll-admin \
                jekyll-feed \
                just-the-docs \
                minima \
                minimal-mistakes-jekyll \
                webrick && \
    apt-get install -y locales && \
    dpkg-reconfigure locales && \
    locale-gen C.UTF-8 && \
    /usr/sbin/update-locale LANG=C.UTF-8 && \
    echo 'en_US.UTF-8 UTF-8' >> /etc/locale.gen && \
    locale-gen

COPY entrypoint /usr/local/bin/entrypoint


ENV LC_ALL C.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

WORKDIR /projects
USER ruby-user
EXPOSE 4000
